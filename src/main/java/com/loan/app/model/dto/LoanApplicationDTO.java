package com.loan.app.model.dto;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;

public class LoanApplicationDTO {
    private Long id;
    private Integer optLock;
    private BigDecimal loanAmount;
    private Integer loanPeriod;
    private OffsetDateTime applicationDate;
    private String applicantName;
    private Boolean wasLoanPeriodExtended;
    private BigDecimal repaymentAmount;
    private OffsetDateTime repaymentDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOptLock() {
        return optLock;
    }

    public void setOptLock(Integer optLock) {
        this.optLock = optLock;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getLoanPeriod() {
        return loanPeriod;
    }

    public void setLoanPeriod(Integer loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    public OffsetDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(OffsetDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Boolean getWasLoanPeriodExtended() {
        return wasLoanPeriodExtended;
    }

    public void setWasLoanPeriodExtended(Boolean wasLoanPeriodExtended) {
        this.wasLoanPeriodExtended = wasLoanPeriodExtended;
    }

    public BigDecimal getRepaymentAmount() {
        return repaymentAmount;
    }

    public void setRepaymentAmount(BigDecimal repaymentAmount) {
        this.repaymentAmount = repaymentAmount;
    }

    public OffsetDateTime getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(OffsetDateTime repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoanApplicationDTO that = (LoanApplicationDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(optLock, that.optLock) && Objects.equals(loanAmount, that.loanAmount) && Objects.equals(loanPeriod, that.loanPeriod) && Objects.equals(applicationDate, that.applicationDate) && Objects.equals(applicantName, that.applicantName) && Objects.equals(wasLoanPeriodExtended, that.wasLoanPeriodExtended) && Objects.equals(repaymentAmount, that.repaymentAmount) && Objects.equals(repaymentDate, that.repaymentDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, optLock, loanAmount, loanPeriod, applicationDate, applicantName, wasLoanPeriodExtended, repaymentAmount, repaymentDate);
    }
}
