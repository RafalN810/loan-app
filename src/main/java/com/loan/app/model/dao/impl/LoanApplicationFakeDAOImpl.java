package com.loan.app.model.dao.impl;

import com.loan.app.model.dao.LoanApplicationDAO;
import com.loan.app.model.entity.LoanApplicationEntity;
import com.loan.app.model.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.ApplicationScope;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Repository
@ApplicationScope
public class LoanApplicationFakeDAOImpl implements LoanApplicationDAO {

    private final List<LoanApplicationEntity> loanApplicationEntityList = new LinkedList<>();
    private long id = 10000;

    @Override
    public LoanApplicationEntity findById(Long id) {
        Optional<LoanApplicationEntity> optionalLoanApplicationEntity = loanApplicationEntityList.stream()
                .filter(loanApplicationEntity -> loanApplicationEntity.getId().equals(id))
                .findFirst();

        return optionalLoanApplicationEntity.orElse(null);
    }

    @Override
    public LoanApplicationEntity create(LoanApplicationEntity loanApplicationEntity) {
        loanApplicationEntity.setId(id);
        id++;
        loanApplicationEntityList.add(loanApplicationEntity);
        return loanApplicationEntity;
    }

    @Override
    public LoanApplicationEntity update(LoanApplicationEntity loanApplicationEntity) throws DaoException {
        LoanApplicationEntity loadedLoanApplicationEntity = findById(loanApplicationEntity.getId());

        if (loadedLoanApplicationEntity == null) {
            throw new DaoException("No entity to update!");
        }

        if (!loadedLoanApplicationEntity.getOptLock().equals(loanApplicationEntity.getOptLock())) {
            throw new DaoException("The database entry has been changed!");
        }

        loadedLoanApplicationEntity.setLoanAmount(loanApplicationEntity.getLoanAmount());
        loadedLoanApplicationEntity.setLoanPeriod(loanApplicationEntity.getLoanPeriod());
        loadedLoanApplicationEntity.setApplicationDate(loanApplicationEntity.getApplicationDate());
        loadedLoanApplicationEntity.setApplicantName(loanApplicationEntity.getApplicantName());
        loadedLoanApplicationEntity.setWasLoanPeriodExtended(loanApplicationEntity.getWasLoanPeriodExtended());
        loadedLoanApplicationEntity.setRepaymentAmount(loanApplicationEntity.getRepaymentAmount());
        loadedLoanApplicationEntity.setRepaymentDate(loanApplicationEntity.getRepaymentDate());
        loadedLoanApplicationEntity.update();

        return loadedLoanApplicationEntity;
    }
}
