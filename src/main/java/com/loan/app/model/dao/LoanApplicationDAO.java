package com.loan.app.model.dao;

import com.loan.app.model.entity.LoanApplicationEntity;
import com.loan.app.model.exception.DaoException;

public interface LoanApplicationDAO {

    LoanApplicationEntity findById(Long id);

    LoanApplicationEntity create(LoanApplicationEntity loanApplicationEntity);

    LoanApplicationEntity update(LoanApplicationEntity loanApplicationEntity) throws DaoException;
}
