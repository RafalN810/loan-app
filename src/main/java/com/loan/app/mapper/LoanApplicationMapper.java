package com.loan.app.mapper;

import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;

public interface LoanApplicationMapper {

    LoanApplicationDTO mapToDTO(LoanApplicationEntity loanApplicationEntity);

    LoanApplicationEntity mapToEntity(LoanApplicationDTO loanApplicationDTO);
}
