package com.loan.app.mapper.impl;

import com.loan.app.mapper.LoanApplicationMapper;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;
import org.springframework.stereotype.Component;

@Component
public class LoanApplicationMapperImpl implements LoanApplicationMapper {
    @Override
    public LoanApplicationDTO mapToDTO(LoanApplicationEntity loanApplicationEntity) {
        if (loanApplicationEntity == null) {
            return null;
        }

        LoanApplicationDTO loanApplicationDTO = new LoanApplicationDTO();
        loanApplicationDTO.setId(loanApplicationEntity.getId());
        loanApplicationDTO.setOptLock(loanApplicationEntity.getOptLock());
        loanApplicationDTO.setLoanAmount(loanApplicationEntity.getLoanAmount());
        loanApplicationDTO.setLoanPeriod(loanApplicationEntity.getLoanPeriod());
        loanApplicationDTO.setApplicationDate(loanApplicationEntity.getApplicationDate());
        loanApplicationDTO.setApplicantName(loanApplicationEntity.getApplicantName());
        loanApplicationDTO.setWasLoanPeriodExtended(loanApplicationEntity.getWasLoanPeriodExtended());
        loanApplicationDTO.setRepaymentAmount(loanApplicationEntity.getRepaymentAmount());
        loanApplicationDTO.setRepaymentDate(loanApplicationEntity.getRepaymentDate());

        return loanApplicationDTO;
    }

    @Override
    public LoanApplicationEntity mapToEntity(LoanApplicationDTO loanApplicationDTO) {
        if (loanApplicationDTO == null) {
            return null;
        }

        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        loanApplicationEntity.setId(loanApplicationDTO.getId());
        loanApplicationEntity.setOptLock(loanApplicationDTO.getOptLock());
        loanApplicationEntity.setLoanAmount(loanApplicationDTO.getLoanAmount());
        loanApplicationEntity.setLoanPeriod(loanApplicationDTO.getLoanPeriod());
        loanApplicationEntity.setApplicationDate(loanApplicationDTO.getApplicationDate());
        loanApplicationEntity.setApplicantName(loanApplicationDTO.getApplicantName());
        loanApplicationEntity.setWasLoanPeriodExtended(loanApplicationDTO.getWasLoanPeriodExtended());
        loanApplicationEntity.setRepaymentAmount(loanApplicationDTO.getRepaymentAmount());
        loanApplicationEntity.setRepaymentDate(loanApplicationDTO.getRepaymentDate());

        return loanApplicationEntity;
    }
}
