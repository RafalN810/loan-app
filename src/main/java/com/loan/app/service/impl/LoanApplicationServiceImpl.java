package com.loan.app.service.impl;

import com.loan.app.mapper.LoanApplicationMapper;
import com.loan.app.model.dao.LoanApplicationDAO;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;
import com.loan.app.model.exception.DaoException;
import com.loan.app.service.LoanApplicationService;
import com.loan.app.service.exception.ServiceException;
import com.loan.app.service.validator.LoanApplicationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;

@Service
public class LoanApplicationServiceImpl implements LoanApplicationService {

    @Value("${com.loan.app.loan.periodExtension:12}")
    private int loanPeriodExtension;

    @Autowired
    private LoanApplicationValidator loanApplicationValidator;

    @Autowired
    private LoanApplicationMapper loanApplicationMapper;

    @Autowired
    private LoanApplicationDAO loanApplicationDAO;

    @Override
    public LoanApplicationDTO createLoanApplication(LoanApplicationDTO loanApplicationDTO) throws ServiceException {
        loanApplicationValidator.validateLoanApplication(loanApplicationDTO);

        LoanApplicationEntity loanApplicationEntity = loanApplicationMapper.mapToEntity(loanApplicationDTO);
        loanApplicationEntity.setRepaymentAmount(loanApplicationDTO.getLoanAmount().multiply(BigDecimal.valueOf(1.07)).setScale(2, RoundingMode.HALF_UP));

        OffsetDateTime repaymentDate = loanApplicationDTO.getApplicationDate().plusMonths(loanApplicationDTO.getLoanPeriod());
        loanApplicationEntity.setRepaymentDate(repaymentDate);
        loanApplicationEntity.setWasLoanPeriodExtended(false);

        return loanApplicationMapper.mapToDTO(
                loanApplicationDAO.create(loanApplicationEntity));
    }

    @Override
    public LoanApplicationDTO extendLoanPeriod(Long id) throws ServiceException {
        LoanApplicationDTO loanApplicationDTO = loanApplicationMapper.mapToDTO(
                loanApplicationDAO.findById(id));

        if (loanApplicationDTO == null) {
            throw new ServiceException("Loan application does not exist!");
        }

        if (loanApplicationDTO.getWasLoanPeriodExtended()) {
            throw new ServiceException("Loan period was already extended!");
        }

        OffsetDateTime newRepaymentDate = loanApplicationDTO.getRepaymentDate().plusMonths(loanPeriodExtension);
        loanApplicationDTO.setRepaymentDate(newRepaymentDate);
        loanApplicationDTO.setWasLoanPeriodExtended(true);

        LoanApplicationEntity loanApplicationEntity = loanApplicationMapper.mapToEntity(loanApplicationDTO);

        try {
            loanApplicationEntity = loanApplicationDAO.update(loanApplicationEntity);
        } catch (DaoException daoException) {
            throw new ServiceException(daoException.getMessage(), daoException);
        }

        return loanApplicationMapper.mapToDTO(loanApplicationEntity);
    }
}
