package com.loan.app.service;

import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.exception.ServiceException;

public interface LoanApplicationService {
    
    LoanApplicationDTO createLoanApplication(LoanApplicationDTO loanApplicationDTO) throws ServiceException;
    
    LoanApplicationDTO extendLoanPeriod(Long id) throws ServiceException;
}
