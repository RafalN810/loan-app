package com.loan.app.service.validator;

import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.OffsetDateTime;

@Component
public class LoanApplicationValidator {

    @Value("${com.loan.app.loan.amountMinimum:1000.0}")
    private BigDecimal loanAmountMin;

    @Value("${com.loan.app.loan.amountMaximum:10000000.0}")
    private BigDecimal loanAmountMax;

    @Value("${com.loan.app.loan.periodMinimum:1}")
    private int loanPeriodMin;

    @Value("${com.loan.app.loan.periodMaximum:36}")
    private int loanPeriodMax;

    public void validateLoanApplication(LoanApplicationDTO loanApplicationDTO) throws ServiceException {
        if (isLoanFake(loanApplicationDTO)) {
            throw new ServiceException("Loan is fake!");
        }

        if (!isLoanAmountValid(loanApplicationDTO)) {
            throw new ServiceException(String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanApplicationDTO.getLoanAmount())));
        }

        if (!isLoanPeriodValid(loanApplicationDTO)) {
            throw new ServiceException(String.format("Loan period: %d is invalid!", loanApplicationDTO.getLoanPeriod()));
        }
    }

    private boolean isLoanFake(LoanApplicationDTO loanApplicationDTO) {
        OffsetDateTime loanApplicationDate = loanApplicationDTO.getApplicationDate();
        OffsetDateTime loanApplicationMidnightDate = OffsetDateTime.of(loanApplicationDate.toLocalDate(), LocalTime.MIDNIGHT, loanApplicationDate.getOffset());
        OffsetDateTime loanApplicationMorningDate = OffsetDateTime.of(loanApplicationDate.toLocalDate(), LocalTime.of(6, 0), loanApplicationDate.getOffset());

        return loanApplicationDTO.getLoanAmount().compareTo(loanAmountMax) == 0 &&
                (loanApplicationDate.compareTo(loanApplicationMidnightDate) >= 0
                        || loanApplicationDate.compareTo(loanApplicationMorningDate) <= 0);
    }

    private boolean isLoanAmountValid(LoanApplicationDTO loanApplicationDTO) {
        return loanApplicationDTO.getLoanAmount().compareTo(loanAmountMin) >= 0 && loanApplicationDTO.getLoanAmount().compareTo(loanAmountMax) <= 0;
    }

    private boolean isLoanPeriodValid(LoanApplicationDTO loanApplicationDTO) {
        return loanApplicationDTO.getLoanPeriod() >= loanPeriodMin && loanApplicationDTO.getLoanPeriod() <= loanPeriodMax;
    }
}
