package com.loan.app.controller;

import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.exception.ServiceException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.OffsetDateTime;

public interface LoanApplicationController {

    @PostMapping("/loanApplication")
    ResponseEntity<LoanApplicationDTO> createLoanApplication(@RequestBody LoanApplicationDTO loanApplicationDTO) throws ServiceException;

    @PatchMapping("/loanApplication/{id}")
    ResponseEntity<OffsetDateTime> extendLoanPeriod(@PathVariable("id") Long id) throws ServiceException;
}
