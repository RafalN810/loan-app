package com.loan.app.controller.impl;

import com.loan.app.controller.LoanApplicationController;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.LoanApplicationService;
import com.loan.app.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;

@RestController
public class LoanApplicationControllerImpl implements LoanApplicationController {

    @Autowired
    private LoanApplicationService loanApplicationService;

    public ResponseEntity<LoanApplicationDTO> createLoanApplication(LoanApplicationDTO loanApplicationDTO) throws ServiceException {
        return ResponseEntity.status(HttpStatus.CREATED).body(loanApplicationService.createLoanApplication(loanApplicationDTO));
    }

    public ResponseEntity<OffsetDateTime> extendLoanPeriod(Long id) throws ServiceException {
        return ResponseEntity.status(HttpStatus.OK).body(loanApplicationService.extendLoanPeriod(id).getRepaymentDate());
    }
}
