package com.loan.app.controller.advice;

import com.loan.app.model.dto.RestExceptionDTO;
import com.loan.app.service.exception.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class LoanApplicationControllerAdvice {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<RestExceptionDTO> handleServiceException(ServiceException serviceException) {
        return buildRequestResponseFromExceptionWithStatus(serviceException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RestExceptionDTO> handleException(Exception exception) {
        return buildRequestResponseFromExceptionWithStatus(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<RestExceptionDTO> buildRequestResponseFromExceptionWithStatus(Exception exception, HttpStatus httpStatus) {
        RestExceptionDTO restExceptionDTO = new RestExceptionDTO();
        restExceptionDTO.setMessage(exception.getMessage());
        restExceptionDTO.setExceptionClass(exception.getClass().getName());
        return ResponseEntity.status(httpStatus).body(restExceptionDTO);
    }
}
