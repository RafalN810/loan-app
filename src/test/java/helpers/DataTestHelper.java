package helpers;

import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class DataTestHelper {

    public static final BigDecimal LOAN_AMOUNT_MIN = BigDecimal.valueOf(1000.0);
    public static final BigDecimal LOAN_AMOUNT_MAX = BigDecimal.valueOf(100000.0);
    public static final int LOAN_PERIOD_MIN = 1;
    public static final int LOAN_PERIOD_MAX = 120;
    public static final int LOAN_PERIOD_EXTENSION = 12;

    public static LoanApplicationEntity getSimpleLoanApplicationEntityWithDiffParameterAndDate(int diffParameter, OffsetDateTime localDateTime) {
        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        loanApplicationEntity.setLoanAmount(BigDecimal.valueOf(1000.0 + diffParameter * 1000.0).setScale(2));
        loanApplicationEntity.setLoanPeriod(12 + diffParameter);
        loanApplicationEntity.setApplicationDate(localDateTime);
        loanApplicationEntity.setApplicantName("mockName" + diffParameter);

        return loanApplicationEntity;
    }

    public static LoanApplicationEntity getLoanApplicationEntityWithParameters(int diffParameter, Long id, OffsetDateTime offsetDateTime, Boolean wasLoanPeriodExtended) {
        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        loanApplicationEntity.setId(id);
        loanApplicationEntity.setLoanAmount(BigDecimal.valueOf(1000.0 + diffParameter * 1000.0).setScale(2));
        loanApplicationEntity.setLoanPeriod(12 + diffParameter);
        loanApplicationEntity.setApplicationDate(offsetDateTime);
        loanApplicationEntity.setApplicantName("mockName" + diffParameter);
        loanApplicationEntity.setWasLoanPeriodExtended(wasLoanPeriodExtended);
        loanApplicationEntity.setRepaymentAmount(BigDecimal.valueOf((1000.0 + diffParameter * 1000.0) * 1.07).setScale(2));
        loanApplicationEntity.setRepaymentDate(offsetDateTime.plusMonths(12 + diffParameter + (wasLoanPeriodExtended ? 12 : 0)));

        return loanApplicationEntity;
    }

    public static LoanApplicationEntity getLoanApplicationEntityWithDiffParameter(int diffParameter) {
        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        loanApplicationEntity.setLoanAmount(BigDecimal.valueOf(1000.0 + diffParameter * 1000.0).setScale(2));
        loanApplicationEntity.setLoanPeriod(12 + diffParameter);
        loanApplicationEntity.setRepaymentDate(OffsetDateTime.of(2021, 1, 21, 15, 0,0,0, ZoneOffset.UTC));
        loanApplicationEntity.setApplicantName("mockName" + diffParameter);
        loanApplicationEntity.setWasLoanPeriodExtended(false);
        loanApplicationEntity.setRepaymentAmount(BigDecimal.valueOf((1000.0 + diffParameter * 1000.0) * 1.07).setScale(2));
        loanApplicationEntity.setRepaymentDate(OffsetDateTime.of(2021, 1, 21, 15, 0,0,0, ZoneOffset.UTC).plusMonths(12 + diffParameter));

        return loanApplicationEntity;
    }

    public static List<LoanApplicationEntity> getDefinedAmountOfLoanApplicationEntityWithDiffParameter(int amount) {
        List<LoanApplicationEntity> loanApplicationEntityList = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            loanApplicationEntityList.add(getLoanApplicationEntityWithDiffParameter(i));
        }

        return loanApplicationEntityList;
    }

    public static LoanApplicationDTO getSimpleLoanApplicationDTOWithDiffParameterAndDate(int diffParameter, OffsetDateTime localDateTime) {
        LoanApplicationDTO loanApplicationDTO = new LoanApplicationDTO();
        loanApplicationDTO.setOptLock(0);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000.0 + diffParameter * 1000.0).setScale(2));
        loanApplicationDTO.setLoanPeriod(12 + diffParameter);
        loanApplicationDTO.setApplicationDate(localDateTime);
        loanApplicationDTO.setApplicantName("mockName" + diffParameter);

        return loanApplicationDTO;
    }

    public static LoanApplicationDTO getLoanApplicationDTOWithParameters(int diffParameter, Long id, OffsetDateTime offsetDateTime, Boolean wasLoanPeriodExtended) {
        LoanApplicationDTO loanApplicationDTO = new LoanApplicationDTO();
        loanApplicationDTO.setId(id);
        loanApplicationDTO.setOptLock(0);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000.0 + diffParameter * 1000.0).setScale(2));
        loanApplicationDTO.setLoanPeriod(12 + diffParameter);
        loanApplicationDTO.setApplicationDate(offsetDateTime);
        loanApplicationDTO.setApplicantName("mockName" + diffParameter);
        loanApplicationDTO.setWasLoanPeriodExtended(wasLoanPeriodExtended);
        loanApplicationDTO.setRepaymentAmount(BigDecimal.valueOf((1000.0 + diffParameter * 1000.0) * 1.07).setScale(2));
        loanApplicationDTO.setRepaymentDate(offsetDateTime.plusMonths(12 + diffParameter + (wasLoanPeriodExtended ? 12 : 0)));

        return loanApplicationDTO;
    }
}
