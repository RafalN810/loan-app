package com.loan.app.service.validator;

import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.exception.ServiceException;
import helpers.DataTestHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LoanApplicationValidatorTest {

    LoanApplicationValidator loanApplicationValidator = new LoanApplicationValidator();

    @BeforeAll
    public void setUp() {
        ReflectionTestUtils.setField(loanApplicationValidator, "loanAmountMin", DataTestHelper.LOAN_AMOUNT_MIN);
        ReflectionTestUtils.setField(loanApplicationValidator, "loanAmountMax", DataTestHelper.LOAN_AMOUNT_MAX);
        ReflectionTestUtils.setField(loanApplicationValidator, "loanPeriodMin", DataTestHelper.LOAN_PERIOD_MIN);
        ReflectionTestUtils.setField(loanApplicationValidator, "loanPeriodMax", DataTestHelper.LOAN_PERIOD_MAX);
    }

    @Test
    void shouldNotThrowExceptionLoanApplicationValid() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);

        // when-then
        assertDoesNotThrow(() -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
    }

    @Test
    void shouldNotThrowExceptionFakeLoanTimePeriodCheck() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 3, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);

        // when-then
        assertDoesNotThrow(() -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
    }

    @Test
    void shouldThrowServiceExceptionLoanFakeMidnight() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 0, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));

        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanFakeMidnightNonUTCTimeZone() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 0, 0, 0, 0, ZoneOffset.of("+02:00"));
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));

        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanFake6am() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 6, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));

        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanFake6amNonUTCTimeZone() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 6, 0, 0, 0, ZoneOffset.of("+02:00"));
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));

        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanFakeBetweenMidnightAnd6am() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 3, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));

        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanFakeBetweenMidnightAnd6amNonUTCTimeZone() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 3, 0, 0, 0, ZoneOffset.of("+04:00"));
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));

        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanAmountBelow() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        BigDecimal loanAmount = BigDecimal.valueOf(500.0).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);

        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanAmountAbove() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        BigDecimal loanAmount = BigDecimal.valueOf(1000000).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);

        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanPeriodBelow() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanPeriod(0);

        String expectedMessage = "Loan period: 0 is invalid!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowServiceExceptionLoanPeriodAbove() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanPeriod(121);

        String expectedMessage = "Loan period: 121 is invalid!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationValidator.validateLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }
}