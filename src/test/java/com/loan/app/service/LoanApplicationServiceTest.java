package com.loan.app.service;

import com.loan.app.mapper.LoanApplicationMapper;
import com.loan.app.model.dao.LoanApplicationDAO;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;
import com.loan.app.model.exception.DaoException;
import com.loan.app.service.exception.ServiceException;
import com.loan.app.service.impl.LoanApplicationServiceImpl;
import com.loan.app.service.validator.LoanApplicationValidator;
import helpers.DataTestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LoanApplicationServiceTest {

    @Mock
    LoanApplicationDAO loanApplicationDAO;

    @Mock
    LoanApplicationMapper loanApplicationMapper;

    @Mock
    LoanApplicationValidator loanApplicationValidator;

    @InjectMocks
    LoanApplicationServiceImpl loanApplicationService;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(loanApplicationService, "loanPeriodExtension", DataTestHelper.LOAN_PERIOD_EXTENSION);
    }

    @Test
    void shouldCreateLoanApplication() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO beforeCreationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        LoanApplicationEntity beforeCreationEntity = DataTestHelper.getSimpleLoanApplicationEntityWithDiffParameterAndDate(0, offsetDateTime);
        Long id = 10000L;
        LoanApplicationEntity createdEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, false);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, false);

        OffsetDateTime expectedRepaymentDate = offsetDateTime.plusMonths(beforeCreationDTO.getLoanPeriod());
        BigDecimal expectedRepaymentAmount = beforeCreationDTO.getLoanAmount().multiply(BigDecimal.valueOf(1.07)).setScale(2);

        doNothing().when(loanApplicationValidator).validateLoanApplication(beforeCreationDTO);
        when(loanApplicationMapper.mapToEntity(beforeCreationDTO)).thenReturn(beforeCreationEntity);
        when(loanApplicationDAO.create(refEq(beforeCreationEntity,
                "applicationDate", "wasLoanPeriodExtended", "repaymentAmount", "repaymentDate"))).thenReturn(createdEntity);
        when(loanApplicationMapper.mapToDTO(createdEntity)).thenReturn(createdDTO);

        ArgumentCaptor<LoanApplicationEntity> loanApplicationEntityArgumentCaptor = ArgumentCaptor.forClass(LoanApplicationEntity.class);

        // when
        LoanApplicationDTO madeLoanApplication = loanApplicationService.createLoanApplication(beforeCreationDTO);

        // then
        verify(loanApplicationValidator, times(1)).validateLoanApplication(beforeCreationDTO);
        verify(loanApplicationMapper, times(1)).mapToEntity(beforeCreationDTO);
        verify(loanApplicationDAO, times(1)).create(loanApplicationEntityArgumentCaptor.capture());

        LoanApplicationEntity capturedEntity = loanApplicationEntityArgumentCaptor.getValue();
        assertEquals(offsetDateTime, capturedEntity.getApplicationDate());
        assertFalse(capturedEntity.getWasLoanPeriodExtended());
        assertEquals(expectedRepaymentDate, capturedEntity.getRepaymentDate());
        assertEquals(expectedRepaymentAmount, capturedEntity.getRepaymentAmount());

        assertEquals(createdDTO, madeLoanApplication);

        verify(loanApplicationMapper, times(1)).mapToDTO(createdEntity);
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFake() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 0, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(100000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        doThrow(new ServiceException("Loan is fake!")).when(loanApplicationValidator).validateLoanApplication(loanApplicationDTO);

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());

        verify(loanApplicationValidator, times(1)).validateLoanApplication(loanApplicationDTO);
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionInvalidLoanAmount() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        BigDecimal loanAmount = BigDecimal.valueOf(500.0).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);
        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));

        doThrow(new ServiceException(String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount))))
                .when(loanApplicationValidator).validateLoanApplication(loanApplicationDTO);

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());

        verify(loanApplicationValidator, times(1)).validateLoanApplication(loanApplicationDTO);
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionInvalidLoanPeriod() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanPeriod(0);
        String expectedMessage = "Loan period: 0 is invalid!";

        doThrow(new ServiceException(String.format("Loan period: 0 is invalid!")))
                .when(loanApplicationValidator).validateLoanApplication(loanApplicationDTO);

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());

        verify(loanApplicationValidator, times(1)).validateLoanApplication(loanApplicationDTO);
    }

    @Test
    void shouldExtendLoanPeriod() throws ServiceException, DaoException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        Long id = 10000L;
        LoanApplicationEntity createdEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, false);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, false);
        LoanApplicationDTO updateDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);
        LoanApplicationEntity updateEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, true);

        when(loanApplicationDAO.findById(id)).thenReturn(createdEntity);
        when(loanApplicationMapper.mapToDTO(createdEntity)).thenReturn(createdDTO);
        when(loanApplicationMapper.mapToEntity(updateDTO)).thenReturn(updateEntity);
        when(loanApplicationDAO.update(updateEntity)).thenReturn(updateEntity);
        when(loanApplicationMapper.mapToDTO(updateEntity)).thenReturn(updateDTO);

        // when
        LoanApplicationDTO extendedPeriodLoanApplication = loanApplicationService.extendLoanPeriod(id);

        // then
        verify(loanApplicationDAO, times(1)).findById(id);
        verify(loanApplicationMapper, times(1)).mapToDTO(createdEntity);
        verify(loanApplicationMapper, times(1)).mapToEntity(updateDTO);
        verify(loanApplicationDAO, times(1)).update(updateEntity);
        verify(loanApplicationMapper, times(1)).mapToDTO(updateEntity);

        assertEquals(updateDTO, extendedPeriodLoanApplication);
    }

    @Test
    void shouldNotExtendLoanPeriodThrowServiceExceptionLoanApplicationNull() {
        // given
        Long id = 10000L;
        when(loanApplicationDAO.findById(id)).thenReturn(null);
        String expectedMessage = "Loan application does not exist!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.extendLoanPeriod(id));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotExtendLoanPeriodThrowServiceExceptionLoanApplicationAlreadyExtended() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        Long id = 10000L;
        LoanApplicationEntity updateEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, true);
        LoanApplicationDTO updateDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);

        when(loanApplicationDAO.findById(id)).thenReturn(updateEntity);
        when(loanApplicationMapper.mapToDTO(updateEntity)).thenReturn(updateDTO);
        String expectedMessage = "Loan period was already extended!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.extendLoanPeriod(id));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotExtendLoanPeriodThrowServiceExceptionLoanUpdateFailed() throws DaoException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        Long id = 10000L;
        LoanApplicationEntity createdEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, false);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, false);
        LoanApplicationDTO updateDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);
        LoanApplicationEntity updateEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, true);

        String expectedMessage = "Update failed!";
        when(loanApplicationDAO.findById(id)).thenReturn(createdEntity);
        when(loanApplicationMapper.mapToDTO(createdEntity)).thenReturn(createdDTO);
        when(loanApplicationMapper.mapToEntity(updateDTO)).thenReturn(updateEntity);
        when(loanApplicationDAO.update(updateEntity)).thenThrow(new DaoException(expectedMessage));

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.extendLoanPeriod(id));
        assertEquals(expectedMessage, thrownException.getMessage());

        verify(loanApplicationDAO, times(1)).findById(id);
        verify(loanApplicationMapper, times(1)).mapToDTO(createdEntity);
        verify(loanApplicationMapper, times(1)).mapToEntity(updateDTO);
        verify(loanApplicationDAO, times(1)).update(updateEntity);
    }
}
