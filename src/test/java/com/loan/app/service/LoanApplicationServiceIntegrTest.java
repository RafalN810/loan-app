package com.loan.app.service;

import com.loan.app.model.dao.LoanApplicationDAO;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.exception.ServiceException;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.annotation.DirtiesContext.*;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class LoanApplicationServiceIntegrTest {

    @Autowired
    LoanApplicationDAO loanApplicationDAO;

    @Autowired
    LoanApplicationService loanApplicationService;

    @Test
    void shouldCreateLoanApplication() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);

        LoanApplicationDTO expectedLoanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);

        // when
        LoanApplicationDTO madeLoanApplication = loanApplicationService.createLoanApplication(loanApplicationDTO);

        // then
        assertNotNull(madeLoanApplication.getId());
        assertEquals(expectedLoanApplicationDTO.getLoanAmount(), madeLoanApplication.getLoanAmount());
        assertEquals(expectedLoanApplicationDTO.getLoanPeriod(), madeLoanApplication.getLoanPeriod());
        assertEquals(expectedLoanApplicationDTO.getApplicationDate(), madeLoanApplication.getApplicationDate());
        assertEquals(expectedLoanApplicationDTO.getApplicantName(), madeLoanApplication.getApplicantName());
        assertEquals(expectedLoanApplicationDTO.getWasLoanPeriodExtended(), madeLoanApplication.getWasLoanPeriodExtended());
        assertEquals(expectedLoanApplicationDTO.getRepaymentAmount(), madeLoanApplication.getRepaymentAmount());
        assertEquals(expectedLoanApplicationDTO.getRepaymentDate(), madeLoanApplication.getRepaymentDate());
    }

    @Test
    void shouldCreateLoanApplicationInFakeLoanTimePeriodCheck() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 3, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);

        LoanApplicationDTO expectedLoanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);

        // when
        LoanApplicationDTO madeLoanApplication = loanApplicationService.createLoanApplication(loanApplicationDTO);

        // then
        assertNotNull(madeLoanApplication.getId());
        assertEquals(expectedLoanApplicationDTO.getLoanAmount(), madeLoanApplication.getLoanAmount());
        assertEquals(expectedLoanApplicationDTO.getLoanPeriod(), madeLoanApplication.getLoanPeriod());
        assertEquals(expectedLoanApplicationDTO.getApplicationDate(), madeLoanApplication.getApplicationDate());
        assertEquals(expectedLoanApplicationDTO.getApplicantName(), madeLoanApplication.getApplicantName());
        assertEquals(expectedLoanApplicationDTO.getWasLoanPeriodExtended(), madeLoanApplication.getWasLoanPeriodExtended());
        assertEquals(expectedLoanApplicationDTO.getRepaymentAmount(), madeLoanApplication.getRepaymentAmount());
        assertEquals(expectedLoanApplicationDTO.getRepaymentDate(), madeLoanApplication.getRepaymentDate());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFakeMidnight() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 0, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFakeMidnightNonUTCTimeZone() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 0, 0, 0, 0, ZoneOffset.of("+02:00"));
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFake6am() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 6, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFake6amNonUTCTimeZone() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 6, 0, 0, 0, ZoneOffset.of("+02:00"));
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFakeBetweenMidnightAnd6am() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 3, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanFakeBetweenMidnightAnd6amNonUTCTimeZone() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 3, 0, 0, 0, ZoneOffset.of("+02:00"));
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));
        String expectedMessage = "Loan is fake!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanAmountBelow() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        BigDecimal loanAmount = BigDecimal.valueOf(400.0).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);
        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanAmountAbove() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        BigDecimal loanAmount = BigDecimal.valueOf(10000000.0).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);
        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanPeriodBelow() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanPeriod(0);
        String expectedMessage = "Loan period: 0 is invalid!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotCreateLoanApplicationThrowServiceExceptionLoanPeriodAbove() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        loanApplicationDTO.setLoanPeriod(121);
        String expectedMessage = "Loan period: 121 is invalid!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.createLoanApplication(loanApplicationDTO));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldExtendLoanPeriod() throws ServiceException {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        Long id = loanApplicationDAO.create(DataTestHelper.getLoanApplicationEntityWithParameters(0, null, offsetDateTime, false)).getId();

        LoanApplicationDTO expectedLoanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);

        // when
        LoanApplicationDTO extendedPeriodLoanApplication = loanApplicationService.extendLoanPeriod(id);

        // then
        assertEquals(expectedLoanApplicationDTO, extendedPeriodLoanApplication);
    }

    @Test
    void shouldNotExtendLoanPeriodThrowServiceExceptionLoanApplicationNull() {
        // given
        Long id = 1000L;
        String expectedMessage = "Loan application does not exist!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.extendLoanPeriod(id));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldNotExtendLoanPeriodThrowServiceExceptionLoanApplicationAlreadyExtended() {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        Long id = loanApplicationDAO.create(DataTestHelper.getLoanApplicationEntityWithParameters(0, null, offsetDateTime, true)).getId();

        String expectedMessage = "Loan period was already extended!";

        // when-then
        ServiceException thrownException = assertThrows(ServiceException.class, () -> loanApplicationService.extendLoanPeriod(id));
        assertEquals(expectedMessage, thrownException.getMessage());
    }
}
