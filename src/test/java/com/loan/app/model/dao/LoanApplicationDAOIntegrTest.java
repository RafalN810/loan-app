package com.loan.app.model.dao;

import com.loan.app.model.entity.LoanApplicationEntity;
import com.loan.app.model.exception.DaoException;
import helpers.DataTestHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.TestInstance.*;
import static org.springframework.test.annotation.DirtiesContext.*;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class LoanApplicationDAOIntegrTest {

    @Autowired
    LoanApplicationDAO loanApplicationDAO;

    @BeforeAll
    void setup() {
        setupData();
    }

    @Test
    void shouldFindById() {
        // given
        BigDecimal expectedAmount = BigDecimal.valueOf(1000.0).setScale(2);
        Integer expectedLoanPeriod = 12;
        String expectedApplicantName = "mockName0";

        // when
        LoanApplicationEntity foundEntity = loanApplicationDAO.findById(10000L);

        // then
        assertNotNull(foundEntity);
        assertEquals(expectedAmount, foundEntity.getLoanAmount());
        assertEquals(expectedLoanPeriod, foundEntity.getLoanPeriod());
        assertEquals(expectedApplicantName, foundEntity.getApplicantName());
    }

    @Test
    void shouldNotFindById() {
        // given

        // when
        LoanApplicationEntity foundEntity = loanApplicationDAO.findById(1000L);

        // then
        assertNull(foundEntity);
    }

    @Test
    void shouldCreate() {
        // given
        LoanApplicationEntity loanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithDiffParameter(1);
        Long expectedId = 10010L;

        Integer expectedOptLock = 0;
        OffsetDateTime expectedCreationDate = OffsetDateTime.now();

        // when
        LoanApplicationEntity createdEntity = loanApplicationDAO.create(loanApplicationEntity);
        LoanApplicationEntity foundEntity = loanApplicationDAO.findById(expectedId);

        // then
        assertNotNull(createdEntity);
        assertEquals(expectedId, createdEntity.getId());

        assertNotNull(foundEntity);
        assertEquals(expectedId, foundEntity.getId());
        assertEquals(loanApplicationEntity.getLoanAmount(), foundEntity.getLoanAmount());
        assertEquals(loanApplicationEntity.getLoanPeriod(), foundEntity.getLoanPeriod());
        assertEquals(loanApplicationEntity.getApplicantName(), foundEntity.getApplicantName());
        assertEquals(expectedOptLock, foundEntity.getOptLock());
        assertEquals(expectedCreationDate.getYear(), foundEntity.getCreationDate().getYear());
        assertEquals(expectedCreationDate.getMonth(),foundEntity.getCreationDate().getMonth());
        assertEquals(expectedCreationDate.getDayOfMonth(), foundEntity.getCreationDate().getDayOfMonth() );
        assertEquals(expectedCreationDate.getHour(), foundEntity.getCreationDate().getHour());
        assertEquals(expectedCreationDate.getMinute(), foundEntity.getCreationDate().getMinute());
    }

    @Test
    void shouldUpdate() throws DaoException {
        // given
        Long id = 10005L;
        LoanApplicationEntity loanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithDiffParameter(11);
        loanApplicationEntity.setId(id);

        Integer expectedOptLock = 1;
        OffsetDateTime expectedModificationDate = OffsetDateTime.now();

        // when
        LoanApplicationEntity updatedEntity = loanApplicationDAO.update(loanApplicationEntity);
        LoanApplicationEntity foundEntity = loanApplicationDAO.findById(id);

        // then
        assertNotNull(updatedEntity);
        assertEquals(id, updatedEntity.getId());

        assertNotNull(foundEntity);
        assertEquals(id, foundEntity.getId());
        assertEquals(loanApplicationEntity.getLoanAmount(), foundEntity.getLoanAmount());
        assertEquals(loanApplicationEntity.getLoanPeriod(), foundEntity.getLoanPeriod());
        assertEquals(loanApplicationEntity.getApplicantName(), foundEntity.getApplicantName());
        assertEquals(expectedOptLock, foundEntity.getOptLock());
        assertEquals(expectedModificationDate.getYear(), foundEntity.getModificationDate().getYear());
        assertEquals(expectedModificationDate.getMonth(),foundEntity.getModificationDate().getMonth());
        assertEquals(expectedModificationDate.getDayOfMonth(), foundEntity.getModificationDate().getDayOfMonth() );
        assertEquals(expectedModificationDate.getHour(), foundEntity.getModificationDate().getHour());
        assertEquals(expectedModificationDate.getMinute(), foundEntity.getModificationDate().getMinute());
    }

    @Test
    void shouldUpdateExtendedLoanApplication() throws DaoException {
        // given
        Long id = 10007L;
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0,0, ZoneOffset.UTC);
        LoanApplicationEntity loanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithParameters(11, id, offsetDateTime, true);

        Integer expectedOptLock = 1;
        OffsetDateTime expectedModificationDate = OffsetDateTime.now();

        // when
        LoanApplicationEntity updatedEntity = loanApplicationDAO.update(loanApplicationEntity);
        LoanApplicationEntity foundEntity = loanApplicationDAO.findById(id);

        // then
        assertNotNull(updatedEntity);
        assertEquals(id, updatedEntity.getId());

        assertNotNull(foundEntity);
        assertEquals(id, foundEntity.getId());
        assertEquals(loanApplicationEntity.getLoanAmount(), foundEntity.getLoanAmount());
        assertEquals(loanApplicationEntity.getLoanPeriod(), foundEntity.getLoanPeriod());
        assertEquals(loanApplicationEntity.getApplicantName(), foundEntity.getApplicantName());
        assertEquals(loanApplicationEntity.getWasLoanPeriodExtended(), foundEntity.getWasLoanPeriodExtended());
        assertEquals(loanApplicationEntity.getRepaymentDate(), foundEntity.getRepaymentDate());
        assertEquals(expectedOptLock, foundEntity.getOptLock());
        assertEquals(expectedModificationDate.getYear(), foundEntity.getModificationDate().getYear());
        assertEquals(expectedModificationDate.getMonth(),foundEntity.getModificationDate().getMonth());
        assertEquals(expectedModificationDate.getDayOfMonth(), foundEntity.getModificationDate().getDayOfMonth() );
        assertEquals(expectedModificationDate.getHour(), foundEntity.getModificationDate().getHour());
        assertEquals(expectedModificationDate.getMinute(), foundEntity.getModificationDate().getMinute());
    }

    @Test
    void shouldThrowAnExceptionWhileUpdateNotFound() {
        // given
        Long id = 1000L;
        LoanApplicationEntity loanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithDiffParameter(11);
        loanApplicationEntity.setId(id);

        String expectedMessage = "No entity to update!";

        // when-then
        DaoException thrownException = assertThrows(DaoException.class, () -> loanApplicationDAO.update(loanApplicationEntity));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    @Test
    void shouldThrowAnExceptionWhileUpdateOptLock() throws DaoException {
        // given
        Long id = 10000L;
        LoanApplicationEntity loanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithDiffParameter(11);
        loanApplicationEntity.setId(id);
        loanApplicationDAO.update(loanApplicationEntity);

        String expectedMessage = "The database entry has been changed!";

        // when-then
        DaoException thrownException = assertThrows(DaoException.class, () -> loanApplicationDAO.update(loanApplicationEntity));
        assertEquals(expectedMessage, thrownException.getMessage());
    }

    private void setupData() {
        List<LoanApplicationEntity> loanApplicationEntityList = DataTestHelper.getDefinedAmountOfLoanApplicationEntityWithDiffParameter(10);
        loanApplicationEntityList.forEach(loanApplicationEntity -> loanApplicationDAO.create(loanApplicationEntity));
    }
}