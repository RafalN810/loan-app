package com.loan.app.mapper;

import com.loan.app.mapper.impl.LoanApplicationMapperImpl;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;

class LoanApplicationMapperTest {

    LoanApplicationMapper loanApplicationMapper = new LoanApplicationMapperImpl();

    @Test
    void shouldMapEntityToDTO() {
        // given
        Long id = 10000L;
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationEntity loanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, true);

        LoanApplicationDTO expectedLoanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);

        // when
        LoanApplicationDTO resultDTO = loanApplicationMapper.mapToDTO(loanApplicationEntity);

        // then
        assertEquals(expectedLoanApplicationDTO, resultDTO);
    }

    @Test
    void shouldMapDTOToEntity() {
        // given
        Long id = 10000L;
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);

        LoanApplicationEntity expectedLoanApplicationEntity = DataTestHelper.getLoanApplicationEntityWithParameters(0, id, offsetDateTime, true);

        // when
        LoanApplicationEntity resultEntity = loanApplicationMapper.mapToEntity(loanApplicationDTO);

        // then
        assertEquals(expectedLoanApplicationEntity, resultEntity);
    }

}