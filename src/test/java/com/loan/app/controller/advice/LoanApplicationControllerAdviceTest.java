package com.loan.app.controller.advice;

import com.loan.app.model.dto.RestExceptionDTO;
import com.loan.app.service.exception.ServiceException;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoanApplicationControllerAdviceTest {

    LoanApplicationControllerAdvice loanApplicationControllerAdvice = new LoanApplicationControllerAdvice();

    @Test
    void shouldHandleServiceException() {
        // given
        ServiceException serviceException = new ServiceException("Mock exception!");

        // when
        ResponseEntity<RestExceptionDTO> dtoResponseEntity = loanApplicationControllerAdvice.handleServiceException(serviceException);

        // then
        assertEquals(HttpStatus.BAD_REQUEST, dtoResponseEntity.getStatusCode());
        assertEquals(serviceException.getMessage(), dtoResponseEntity.getBody().getMessage());
        assertEquals(serviceException.getClass().getName(), dtoResponseEntity.getBody().getExceptionClass());
    }

    @Test
    void shouldHandleNonServiceException() {
        // given
        NullPointerException nullPointerException = new NullPointerException("Mock exception!");

        // when
        ResponseEntity<RestExceptionDTO> dtoResponseEntity = loanApplicationControllerAdvice.handleException(nullPointerException);

        // then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, dtoResponseEntity.getStatusCode());
        assertEquals(nullPointerException.getMessage(), dtoResponseEntity.getBody().getMessage());
        assertEquals(nullPointerException.getClass().getName(), dtoResponseEntity.getBody().getExceptionClass());
    }
}