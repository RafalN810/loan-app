package com.loan.app.controller.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loan.app.model.dao.LoanApplicationDAO;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.dto.RestExceptionDTO;
import com.loan.app.service.exception.ServiceException;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.annotation.DirtiesContext.ClassMode;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class LoanApplicationControllerAdviceIntegrTest {

    String basePath = "/loanApplication";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    LoanApplicationDAO loanApplicationDAO;

    @Test
    void shouldHandleServiceExceptionWhenCreatingLoanApplicationAndLoanFake() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 0, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);
        loanApplicationDTO.setLoanAmount(BigDecimal.valueOf(1000000.0).setScale(2));

        String expectedMessage = "Loan is fake!";
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(loanApplicationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenCreatingLoanApplicationAndLoanAmountAbove() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);
        BigDecimal loanAmount = BigDecimal.valueOf(10000000.0).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);

        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(loanApplicationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenCreatingLoanApplicationAndLoanAmountBelow() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);
        BigDecimal loanAmount = BigDecimal.valueOf(400.0).setScale(2);
        loanApplicationDTO.setLoanAmount(loanAmount);

        String expectedMessage = String.format("Loan amount: %s is invalid!", new DecimalFormat("0.00").format(loanAmount));
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(loanApplicationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenCreatingLoanApplicationAndLoanPeriodAbove() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);
        Integer loanPeriod = 121;
        loanApplicationDTO.setLoanPeriod(loanPeriod);

        String expectedMessage = String.format("Loan period: %s is invalid!", loanPeriod);
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(loanApplicationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenCreatingLoanApplicationAndLoanPeriodBelow() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);
        Integer loanPeriod = 0;
        loanApplicationDTO.setLoanPeriod(loanPeriod);

        String expectedMessage = String.format("Loan period: %s is invalid!", loanPeriod);
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(loanApplicationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenExtendingLoanPeriodLoanApplicationNull() throws Exception {
        // given
        Integer id = 1;

        String expectedMessage = "Loan application does not exist!";
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                patch(basePath + "/" + id))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenExtendingLoanPeriodLoanApplicationExtended() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        Long id = loanApplicationDAO.create(DataTestHelper.getLoanApplicationEntityWithParameters(0, null, offsetDateTime, true)).getId();

        String expectedMessage = "Loan period was already extended!";
        String expectedExceptionClass = ServiceException.class.getName();

        // when
        MockHttpServletResponse response = mockMvc.perform(
                patch(basePath + "/" + id))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(expectedMessage, restExceptionDTO.getMessage());
        assertEquals(expectedExceptionClass, restExceptionDTO.getExceptionClass());
    }
}