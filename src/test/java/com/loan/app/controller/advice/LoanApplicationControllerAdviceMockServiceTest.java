package com.loan.app.controller.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loan.app.controller.LoanApplicationController;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.dto.RestExceptionDTO;
import com.loan.app.service.LoanApplicationService;
import com.loan.app.service.exception.ServiceException;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.annotation.DirtiesContext.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(LoanApplicationController.class)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class LoanApplicationControllerAdviceMockServiceTest {

    String basePath = "/loanApplication";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    LoanApplicationService loanApplicationService;

    @Test
    void shouldHandleServiceExceptionWhenCreatingLoanApplicationFailed() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO beforeCreationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);

        ServiceException serviceException = new ServiceException("Mock exception!");
        when(loanApplicationService.createLoanApplication(beforeCreationDTO)).thenThrow(serviceException);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(beforeCreationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(serviceException.getMessage(), restExceptionDTO.getMessage());
        assertEquals(serviceException.getClass().getName(), restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleServiceExceptionWhenExtendingLoanPeriodFailed() throws Exception {
        // given
        Long id = 10000L;

        ServiceException serviceException = new ServiceException("Mock exception!");
        when(loanApplicationService.extendLoanPeriod(id)).thenThrow(serviceException);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                patch(basePath + "/" + id))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_BAD_REQUEST, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(serviceException.getMessage(), restExceptionDTO.getMessage());
        assertEquals(serviceException.getClass().getName(), restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleNonServiceWhenCreatingLoanApplicationFailed() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO beforeCreationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);

        NullPointerException nullPointerException = new NullPointerException("Mock exception!");
        when(loanApplicationService.createLoanApplication(beforeCreationDTO)).thenThrow(nullPointerException);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(beforeCreationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_INTERNAL_SERVER_ERROR, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(nullPointerException.getMessage(), restExceptionDTO.getMessage());
        assertEquals(nullPointerException.getClass().getName(), restExceptionDTO.getExceptionClass());
    }

    @Test
    void shouldHandleNonServiceExceptionWhenExtendingLoanPeriodFailed() throws Exception {
        // given
        Long id = 10000L;

        NullPointerException nullPointerException = new NullPointerException("Mock exception!");
        when(loanApplicationService.extendLoanPeriod(id)).thenThrow(nullPointerException);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                patch(basePath + "/" + id))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_INTERNAL_SERVER_ERROR, response.getStatus());
        RestExceptionDTO restExceptionDTO = objectMapper.readValue(response.getContentAsString(), RestExceptionDTO.class);
        assertEquals(nullPointerException.getMessage(), restExceptionDTO.getMessage());
        assertEquals(nullPointerException.getClass().getName(), restExceptionDTO.getExceptionClass());
    }

}