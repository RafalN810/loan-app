package com.loan.app.controller;

import com.loan.app.controller.impl.LoanApplicationControllerImpl;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.LoanApplicationService;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanApplicationControllerTest {

    @Mock
    LoanApplicationService loanApplicationService;

    @InjectMocks
    LoanApplicationControllerImpl loanApplicationController;

    @Test
    void shouldCreateLoanApplication() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO beforeCreationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, 10000L, offsetDateTime, false);

        when(loanApplicationService.createLoanApplication(beforeCreationDTO)).thenReturn(createdDTO);

        // when
        ResponseEntity<LoanApplicationDTO> dtoResponseEntity = loanApplicationController.createLoanApplication(beforeCreationDTO);

        // then
        assertEquals(HttpStatus.CREATED, dtoResponseEntity.getStatusCode());
        assertEquals(createdDTO, dtoResponseEntity.getBody());
    }

    @Test
    void shouldExtendLoanPeriod() throws Exception {
        // given
        Long id = 10000L;
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);

        when(loanApplicationService.extendLoanPeriod(id)).thenReturn(createdDTO);

        // when
        ResponseEntity<OffsetDateTime> dtoResponseEntity = loanApplicationController.extendLoanPeriod(id);

        // then
        assertEquals(HttpStatus.OK, dtoResponseEntity.getStatusCode());
        assertEquals(createdDTO.getRepaymentDate(), dtoResponseEntity.getBody());
    }
}
