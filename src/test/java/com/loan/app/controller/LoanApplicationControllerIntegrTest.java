package com.loan.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loan.app.model.dao.LoanApplicationDAO;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.model.entity.LoanApplicationEntity;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.annotation.DirtiesContext.ClassMode;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class LoanApplicationControllerIntegrTest {

    String basePath = "/loanApplication";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    LoanApplicationDAO loanApplicationDAO;

    @Test
    void shouldCreateLoanApplication() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO loanApplicationDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, null, offsetDateTime, false);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(loanApplicationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_CREATED, response.getStatus());
        LoanApplicationDTO responseLoanApplicationDTO = objectMapper.readValue(response.getContentAsString(), LoanApplicationDTO.class);
        assertNotNull(responseLoanApplicationDTO.getId());
        assertEquals(loanApplicationDTO.getLoanAmount(), responseLoanApplicationDTO.getLoanAmount());
        assertEquals(loanApplicationDTO.getLoanPeriod(), responseLoanApplicationDTO.getLoanPeriod());
        assertEquals(loanApplicationDTO.getApplicationDate(), responseLoanApplicationDTO.getApplicationDate());
        assertEquals(loanApplicationDTO.getApplicantName(), responseLoanApplicationDTO.getApplicantName());
        assertEquals(loanApplicationDTO.getWasLoanPeriodExtended(), responseLoanApplicationDTO.getWasLoanPeriodExtended());
        assertEquals(loanApplicationDTO.getRepaymentAmount(), responseLoanApplicationDTO.getRepaymentAmount());
        assertEquals(loanApplicationDTO.getRepaymentDate(), responseLoanApplicationDTO.getRepaymentDate());
    }

    @Test
    void shouldExtendLoanPeriod() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationEntity loanApplicationEntity = loanApplicationDAO.create(DataTestHelper.getLoanApplicationEntityWithParameters(0, null, offsetDateTime, false));
        Long id = loanApplicationEntity.getId();

        OffsetDateTime expectedRepaymentDate = loanApplicationEntity.getRepaymentDate().plusMonths(12);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                patch(basePath + "/" + id))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_OK, response.getStatus());
        OffsetDateTime offsetDateTimeResponse = objectMapper.readValue(response.getContentAsString(), OffsetDateTime.class);
        assertEquals(expectedRepaymentDate, offsetDateTimeResponse);
    }
}
