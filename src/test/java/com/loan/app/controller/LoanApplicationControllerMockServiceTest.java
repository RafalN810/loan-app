package com.loan.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loan.app.model.dto.LoanApplicationDTO;
import com.loan.app.service.LoanApplicationService;
import helpers.DataTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.annotation.DirtiesContext.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(LoanApplicationController.class)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class LoanApplicationControllerMockServiceTest {

    String basePath = "/loanApplication";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    LoanApplicationService loanApplicationService;

    @Test
    void shouldCreateLoanApplication() throws Exception {
        // given
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO beforeCreationDTO = DataTestHelper.getSimpleLoanApplicationDTOWithDiffParameterAndDate(0, offsetDateTime);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, 10000L, offsetDateTime, false);

        when(loanApplicationService.createLoanApplication(beforeCreationDTO)).thenReturn(createdDTO);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                post(basePath)
                        .content(objectMapper.writeValueAsString(beforeCreationDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_CREATED, response.getStatus());
        LoanApplicationDTO responseLoanApplicationDTO = objectMapper.readValue(response.getContentAsString(), LoanApplicationDTO.class);
        assertEquals(createdDTO, responseLoanApplicationDTO);
    }

    @Test
    void shouldExtendLoanPeriod() throws Exception {
        // given
        Long id = 10000L;
        OffsetDateTime offsetDateTime = OffsetDateTime.of(2021, 5, 3, 12, 0, 0, 0, ZoneOffset.UTC);
        LoanApplicationDTO createdDTO = DataTestHelper.getLoanApplicationDTOWithParameters(0, id, offsetDateTime, true);

        when(loanApplicationService.extendLoanPeriod(id)).thenReturn(createdDTO);

        // when
        MockHttpServletResponse response = mockMvc.perform(
                patch(basePath + "/" + id))
                .andReturn()
                .getResponse();

        // then
        assertEquals(MockHttpServletResponse.SC_OK, response.getStatus());
        OffsetDateTime offsetDateTimeResponse = objectMapper.readValue(response.getContentAsString(), OffsetDateTime.class);
        assertEquals(createdDTO.getRepaymentDate(), offsetDateTimeResponse);
    }
}
